from datetime import datetime
from flask import Blueprint
from flask import render_template
from flask import session
from flask import redirect
from flask import request
from flask import g
from flask import flash
from ..forms import reg_form
from ..models import user as UserRef
from functools import wraps

mod = Blueprint('default', __name__)

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 'username' not in session:
            return redirect('/login')
        else:
            return func(*args, **kwargs)
    return wrapper 

@mod.route('/')
def index():
    if 'username' not in session:
        return redirect('/login')
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    items = ["Book1", "Book2", "Book3", "Book4"]
    return render_template('default/index.html', server_time=current_time, items=items)

@mod.route('/login', methods=['GET'])
def login_page():
    if 'username' in session:
        return redirect('/')
    return render_template('default/login.html')

@mod.route('/login', methods=['POST'])
def login_submit():
    username = request.form['username']
    password = request.form['password']
    if g.usersdb.getUserWithPassword(username, password).count() > 0:
        session['username'] = username
        return redirect('/')
    else:
        flash('Invalid username and password.', 'signin_failure')
        return redirect('/login') 

@mod.route('/logout', methods=['GET'])
def logout_submit():
    session.pop('username', None)
    session.clear()
    return redirect('/')

@mod.route('/register', methods=['GET', 'POST'])
def register():
    form = reg_form.RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = UserRef.User(form.username.data, form.email.data,
                    form.password.data)
        g.usersdb.createUserUsingUser(user)
        session['username'] = form.username.data
        return redirect('/')
    return render_template('default/signup.html', form=form)

@mod.route('/cart')
def cart():
    if 'username' not in session:
        return redirect('/login')
    return render_template('default/cart.html')
